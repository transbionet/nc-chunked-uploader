# NextCloud chunked uploader

## Introduction

This repo contains the script developed to upload humongous files or directories full of them to a Nextcloud/B2DROP instance by chunks, and then assembling it on the Nextcloud side.

## Dependencies

This script needs next command line dependencies:

* bash

* coreutils (`dd`, `stat`, `printf`, `nproc`, etc...)

* curl

* [OPTIONAL] `sem` command from [parallel](https://www.gnu.org/software/parallel/) package, to orchestrate parallel uploads.

## Setup file
A setup file like the [template one provided in this repo](setup.ini.template) should be created with the server, the destination path
(it will be created when it does not exist) and the credentials.

## Usage

```
bash ./ncChunkedUploader.bash {setup_file} file_or_dir_1 file_or_dir_2 file_or_dir_3 .....
```

